<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;


/**
 * This is the model class for table "movies".
 *
 * @property int $id
 * @property string $title
 * @property string $photo
 * @property int $year
 */
class Movies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year', 'title'], 'required'],
            [['year'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['photo'], 'default', 'value' => NULL]

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'photo' => 'Photo',
            'year' => 'Year',
        ];
    }

    public function getImageUrl($id)
    {
        return Url::to('@web/imgs/' . $id.'.jpg',
            true);
    }

    public function upload()
    {
        if ($this->validate()) {
            $id = Movies::getLasId()->id +1;
            $this->photo->saveAs('imgs/' . $id . '.jpg');
            return true;
        } else {
            return false;
        }
    }
    public function uploadid($id)
    {
        if ($this->validate()) {
            $this->photo->saveAs('imgs/' . $id . '.jpg');
            return true;
        } else {
            return false;
        }
    }
    public function getLasId(){
        return $id=Movies::find()
            ->select('id')
            ->orderBy(['id' =>SORT_DESC])
            ->limit(1)
            ->one();
    }
    public function  editModel($model){
        $id = Movies::getLasId()->id +1;
        $model->name = $id.'.jpg';
        return $model;

    }

    public function find_movie($model){

        $consulta= self::find()
            ->where(['like','title', $model]);

        return $consulta;

    }
}
