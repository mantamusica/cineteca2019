<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Movies;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Movies();
        if ($model->load(Yii::$app->request->post())) {
            $dataProvider = new ActiveDataProvider([
                'query' => Movies::find_movie($model->title),
                'pagination' => [
                    'pageSize' => 4
                ]
            ]);


        }else{
            $dataProvider = new ActiveDataProvider([
                'query' => Movies::find(),
                'pagination' => [
                    'pageSize' => 4
                ]
            ]);
        }

        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);

    }

    public function actionShow($id)
    {

        $movie=Movies::find()
            ->where(['id' => $id])->one();

        return $this->render("detail_movie",[
            "data"=>$movie,
            'title' => 'Movie',
        ]);
    }




}
