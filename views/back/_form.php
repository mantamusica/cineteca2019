<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Movies */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="movies-form">
    <div class="row col-lg-6">
        <?php $form = ActiveForm::begin([
                'options' => [
                        'enctype' => 'multipart/form-data',
                        'validateOnSubmit' => true,
                ]]) ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'placeholder' => 'Introduce the title of the movie']) ?>

        <?= $form->field($model, 'year')->textInput(['placeholder' => 'Introduce the title of the movie'])?>

        <?= $form->field($model, 'photo')->fileInput(['class' => 'btn btn-default btn-file']) ?>


        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            <?= Html::a('Atrás', ['/back'], ['class'=>'btn btn-danger']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="row col-lg-6">
        <?php if($model->id != NULL){
            if($model->photo == NULL ){
                $url = $model->getImageUrl(0);
            }else{
                $url = $model->getImageUrl($model->id);
            }
            ?>
            <div class="col-md-12">
                <div class="thumbnail">
                    <?= Html::img($url, ['width'=>'300px','height'=>'425px','display' =>'block', 'margin' =>'auto']); ?>
                </div>
            </div>
            <br>
            <?php
        }
        ?>
    </div>
</div>
