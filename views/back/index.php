<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Movies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movies-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::button('Create Movies', ['value' => Url::to('create'), 'class' => 'btn btn-success', 'id'=>'modalButton']) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>New Movie</h4>',
        'id' => 'modal'
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],

        'layout' => "{pager}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'year',
            [
                'header' => 'Cartel',
                'format'=>'html',
                'value' => function ($model) {
                    if($model->photo == NULL){
                        $url = $model->getImageUrl(0);
                    }else{
                        $url = $model->getImageUrl($model->id);
                    }
                    return Html::img($url, ['width'=>'30','height'=>'50','display' =>'block', 'margin' =>'auto']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',],
        ],
    ]); ?>


</div>
