<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use app\models\Movies;
$url = $model->getImageUrl($model->id);
/* @var $this yii\web\View */

$this->title = 'Movies';

?>
<div class="site-index">
    <div class="jumbotron">
        <h1><?= $title?></h1>
    </div>
    <div class="container">
        <h1></h1>
        <div class="row">
            <div class="col-md-9">
                <div class="thumbnail">
                    <?= Html::img($url, [
                            'width'=>'300px',
                            'height'=>'425px',
                            'display' =>'block',
                            'margin' =>'auto',
                            ['class' => 'img-responsive img-thumbnail']
                    ]); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="thumbnail">
                    <h2>ID : <?= $model->id ?></h2>
                    <hr>
                    <h2>Title : <?= $model->title ?></h2>
                    <hr>
                    <h3>Year : <?= $model->year ?></h3>
                    <hr>
                    <h3>Photo File : <?= $model->photo ?></h3>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row center-block"><?= Html::a('Atrás', ['/back'], ['class'=>'btn btn-danger']) ?></div>
</div>
