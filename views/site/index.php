<?php

use app\models\Movies;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Modal;

//
///* @var $this yii\web\View */
///
$this->title = 'My Yii Application';
?>

<div class="site-index">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <?php $form = ActiveForm::begin(['options' => ['validateOnSubmit' => true]]); ?>
                <?= $form->field($model, 'title')->textInput(['placeholder' => 'Introduce the title of the movie'])?>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary pull-right', 'name' => 'title']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <?php
    Modal::begin(['header' => '<h4>New Movie</h4>', 'id' => 'modal']);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{pager}\n{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel' => 'Última',
            'prevPageLabel' => 'Anterior',
            'nextPageLabel' => 'Siguiente',
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'year',
            [
                'header' => 'Cartel',
                'format'=>'html',
                'value' => function ($model) {
                    $url = $model->getImageUrl($model->id);
                    return Html::img($url, ['width'=>'80','height'=>'120','display' =>'block', 'margin' =>'auto']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Información',
                'template' => '{link}',

                'buttons' => [
                    'link' => function ($url,$model) {
                        $url = 'show?id=' . $model->id;
                        return Html::button('Ampliar Información', ['value' => $url, 'class' => 'btn btn-danger', 'id'=>'modalButton']);
                     },
                ],
            ],
        ],
    ]);
    ?>
</div>



